# Docker Solidner Bookstack

| DOCKER TAG                                  | VERSION BOOKSTACK  | UPDATE DATE | STATUS |
|---------------------------------------------|--------------------|--------|--------|
| bcaata/bookstack:23.5.0-solidnerd           | 23.5.0             | 08 FEB 2024 |
| bcaata/bookstack:23.5.0-solidnerd-ssl       | 23.5.0             | 08 FEB 2024 |
| bcaata/bookstack:23.5.0-solidnerd-12.02.24  | 23.5.0             | 12 FEB 2024 | (latest) |